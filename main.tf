provider "aws" {
    version = "~> 2.0"
    region  = "eu-west-2"
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

owners = ["099720109477"] # Canonical
}
resource "aws_instance" "hello_world" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t3.small"

    tags = {
        Name = "Hello_World"
    }
}